import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator

dag = DAG(
    "test_data",
    schedule_interval=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
)

t1 = BashOperator(
    task_id="bash_run",
    bash_command="echo value hello",
    dag=dag,
)

t1